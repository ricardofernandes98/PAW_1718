<?php 

session_start();

if (!isset($_SESSION['login']) || empty($_GET['username'])) {

    header('Location: ../Views/index.php');

} else {

    include_once '../Modules/User.php';

    // Obter o valor do username
    $username = $_GET['username'];

    $user = new User();

    // Eliminar o utilizador selecionado
    $user->deleteUser($username);

    header('Location: ../Views/users.php');

}