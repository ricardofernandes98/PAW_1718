<?php

/* 
 * Caso se tente acessar a este ficheiro diretamente pelo url o utilizador é enviado
 * para a página incial
 */ 
if (empty($_POST)) {

    header('Location: ../Views/index.php');

} else {

    include_once '../Modules/User.php';

    // Obter os valores do login
    $username = $_POST['username'];
    $password = md5($username.$_POST['password']);

    $user = new User();

    // Querie para obter o utizador na base de dados
    $query = "SELECT username, password FROM users WHERE username = '".$username."' AND password = '".$password."'";

    /**
     * Se não for encontrado o utilizador, o login não é efetuado. Caso seja encontrado significa
     * que a sessão pode ser iniciada e o nome da sessão corresponde ao username do utilizador 
     */
    if (empty ($user->query($query))) {

        include_once '../Views/login.php';
        echo '<footer class="login-error">';
        echo '<span>Username ou palavra-passe incorretos</span>';
        echo '<footer>';

    } else {

        // Iniciar sessão
        session_start();

        // Variáveis da sessão
        $_SESSION['username'] = $username;
        $_SESSION['login'] = 1;

        header('Location: ../Views/index.php');

    }

}