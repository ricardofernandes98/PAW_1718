<?php

session_start();

/* 
 * Caso se tente acessar a este ficheiro diretamente pelo url o utilizador é enviado
 * para a página incial
 */ 
if (empty($_POST)) {

    header('Location: ../Views/index.php');

} else {

    // Limpar as variáveis da sessão
    session_unset();

    // Destruir a sessão
    session_destroy();  

    header('Location: ../Views/index.php');

}    