<?php

/* 
 * Caso se tente acessar a este ficheiro diretamente pelo url o utilizador é enviado
 * para a página incial
 */ 
if (empty($_POST)) {

    header('Location: ../Views/index.php');

} else {

    include_once '../Modules/User.php';

    // Caso a confirmação da palavra passe não seja compatível, tem de ser novamente inserida
    if ($_POST['password'] !== $_POST['confirm_password']) {
        
        include_once '../Views/registo.php';
        echo '<footer class="regist-error">';
        echo '<span>As palavras-passe não coincidem</span>';
        echo '<footer>';

    } else {
        
        // Obter as informações do registo
        $username  = $_POST['username'];
        $firstname = $_POST['firstname'];
        $lastname  = $_POST['lastname'];
        $email     = $_POST['email'];
        $password  = md5($username.$_POST['password']);

        // Criação do utilizador
        $user = new User();

        // Verificação se o utilizador já existe. Caso exista o registo tem de ser repetido
        if (!empty ($user->getUser($username))) {

            include_once '../Views/registo.php';
            echo '<footer class="regist-error">';
            echo '<span>O utilizador já existe</span>';
            echo '<footer>';

        } else {
            
            // Definir os valores dos atributos dos utilizadores
            $user->setUsername($username);
            $user->setFirstname($firstname);
            $user->setLastname($lastname);
            $user->setEmail($email);
            $user->setPassword($password);

            // Inserir o utlizador na base de dados
            $user->addToDatabase();

            // Redirecionar para a página de login
            header('Location: ../Views/login.php');
    
        }
        
    }

}
