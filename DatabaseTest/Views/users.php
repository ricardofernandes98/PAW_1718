<?php
    session_start();

    // Nome da página
    $page = 'Lista de utilizadores';
    
    // Incluir o topo
    include_once 'topo.php';

    // Incluir o ficheiro que obtém os utilizadores da base de dados
    include_once '../Controllers/users.php';
?>
    <!-- Corpo principal da página de utilizadores -->

        <div class="users-content">

        <?php if (!isset($_SESSION['login'])) { ?>

            <h3>Informação disponivél apenas para utilizadores com a sessão inciada. 
                <a href="<?= $url_site ?>Views/login.php">Efetue o seu login</a>.
            </h3>

        <?php } else { ?>       
        
            <table class="users-table" align="center">
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>

            <!-- Enquanto houver utilizadores no array são criadas novas linhas com a informação -->
            <?php for ($i = 0; $i < count($users); $i++) { ?>

                <tr>
                    <td><?= $users[$i]['id'] ?></td>
                    <td><?= $users[$i]['username'] ?></td>
                    <td><?= $users[$i]['firstName'] ?></td>
                    <td><?= $users[$i]['lastName'] ?></td>
                    <td><?= $users[$i]['emai'] ?></td>
                    <td>
                        <form action="<?= $url_site ?>Controllers/test.php" method="GET">
                            <input type="hidden" name="username" value="<?= $users[$i]['username'] ?>">
                            <input type="submit" name="Editar" value="Editar">
                        </form>    
                    </td>

                <?php if ($users[$i]['username'] !== $_SESSION['username']) { ?>

                    <td>
                        <form action="<?= $url_site ?>Controllers/deleteUser.php" method="GET">
                            <input type="hidden" name="username" value="<?= $users[$i]['username'] ?>">
                            <input type="submit" name="Eliminar" value="Eliminar">
                        </form>    
                    </td>

                <?php } else { ?>

                    <td>Sessão iniciada</td>

                <?php } ?>    

                </tr>

            <?php } ?>

            </table>

        <?php } ?>    

        </div>

    </body>

</html>