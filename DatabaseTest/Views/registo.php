<?php
    session_start();

    // Nome da página
    $page = 'Registo';
    
    // Incluir o topo
    include_once 'topo.php';
?>
    <!-- Corpo principal da página de registo -->

        <div class="registo-page">

        <?php if (isset($_SESSION['login'])) { ?>

            <h3>Já se encontra registado.</h3>

        <?php } else { ?>   

            <header class="registo-title">
                <h1>Faça o seu registo</h1>
            </header>

            <div class="registo-content">

                <form action="<?= $url_site ?>Controllers/registo.php" method="post">
                    <span>Username:</span>
                    <input type="text" name="username" placeholder="Username" maxlength="20" required>
                    <br>
                    <span>First name:</span>
                    <input type="text" name="firstname" placeholder="First Name" maxlength="20" required>
                    <br>
                    <span>Last name:</span>
                    <input type="text" name="lastname" placeholder="Last Name" maxlength="20" required>
                    <br>
                    <span>Email:</span>
                    <input type="email" name="email" placeholder="Email" maxlength="100" required>
                    <br>
                    <span>Password:</span>
                    <input type="password" name="password" placeholder="Password" maxlength="30" required>
                    <br>
                    <span>Confirm password:</span>
                    <input type="password" name="confirm_password" placeholder="Confirm password" maxlength="30" required>
                    <br>
                    <button type="submit">Submeter dados</button>
                </form>
                
            <div>

        <?php } ?>    

        </div>

    </body>

</html>