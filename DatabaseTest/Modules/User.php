<?php

// Incluir a base de dados
include_once '../Modules/Database.php';

/**
 * Classe que representa um utilizador
 */
class User {

    private $username;
    private $firstname;
    private $lastname;
    private $email;
    private $password;

    /**
     * Get the value of username
     */ 
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */ 
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of firstname
     */ 
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname
     *
     * @return  self
     */ 
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of lastname
     */ 
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname
     *
     * @return  self
     */ 
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Adicionar o utilizador na base de dados
     */
    public function addToDatabase() {
        $query = "INSERT INTO `users` (`id`, `username`, `firstName`, `lastName`, `emai`, `password`) 
                  VALUES (NULL, '".$this->getUsername()."','".$this->getFirstname()."','".$this->getLastname()."','".$this->getEmail()."', '".$this->getPassword()."')";

        query($query);        
    }

    /**
     * Obter um utilizador da base de dados em função do username
     */
    public function getUser($username) {
        $query = "SELECT * FROM users WHERE username = '".$username."'";
        return query($query);
    }

    /**
     * Eliminar um utilizador da base de dados em função do username
     */
    public function deleteUser($username) {
        $query = "DELETE FROM users WHERE username = '".$username."'";
        return query($query);
    }

    /**
     * Obter todos os utilizadores da base de dados
     */
    public function getAllUsers() {
        $query = "SELECT * FROM users";
        return query($query);
    }

    /**
     * Obter uma informação à escolha
     */
    public function query($query) {
        return query($query);
    }
}