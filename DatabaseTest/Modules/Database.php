<?php

// Criar a connecção com a base de dados
function connect() {
    $pdo = new PDO("mysql:host=localhost;dbname=DBExample", "root", null);
    return $pdo;
}

// Fechar a connecção com a base de dados 
function close_connection($connection) {
    $connection = null;
}

// Executar uma querie na base de dados
function query($query) {
    $statement = connect()->query($query);

    if (explode(' ', $query)[0] == 'SELECT') {
        $data = $statement->fetchAll();
        close_connection(connect());
        return $data;
    }

    close_connection(connect());
}
