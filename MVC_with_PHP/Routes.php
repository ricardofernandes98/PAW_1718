<?php

// Home page
Route::set('index.php', function() {
    Home::CreateView('Home');   
});

// Login page
Route::set('login', function() {
    Login::CreateView('Login');
});

// Regist page
Route::set('regist', function() {
    Registo::CreateView('Registo');
});

// All users page
Route::set('show-users', function() {
    Users::CreateView('Users');
});

// Edit user page
Route::set('edit-user', function() {
    Users::CreateView('User');
});

// Login user
Route::set('login-user', function() {
    // Caso não haja nenhum dados por POST
    if (empty($_POST)) {
        // Não pode haver login. Redirecionar para a página inicial
        header('Location: index.php');
    } else {
        // Verificação de login
        Login::checkLogin($_POST['username'], $_POST['password']);
    }

});

// Login error
Route::set('login-err', function() {   
    // Se houver sessão inciada não existe erro de login
    // Redirecionar para a página inicial
    if (isset($_SESSION['login'])) {
        header('Location: index.php');
    } else {
        Login::CreateView('Login-err');
    }

});

// Logout
Route::set('logout', function() {
    session_start();

    // Se a sessão estiver iniciada pode ser efetuado o logout
    if(isset($_SESSION['login']) && !empty($_POST['username'])) {
        Session::logout();
    } else {
        header('Location: index.php');
    }

});