<?php

/**
 * Super class Controller
 */
class Controller {
    
    /**
     * Função que permite a criação de uma view
     */
    public static function CreateView($view) {
        require_once './Views/'.$view.'.php';
    }

}