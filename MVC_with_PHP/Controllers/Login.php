<?php

class Login extends Controller {
    
    /**
     * Função que verifica se os dados de login estão corretos, 
     * caso estejam, a sessão é inciada
     */
    public static function checkLogin($username, $password) {
        // Encontrar o utilizador consoante o username e password
        $user = Users::findOne($username, $password);

        // Caso não seja encontrado nenhum utilizador na base de dados
        if (empty($user)) {

            // Redirecionar para a página de erro de login
            header('Location: login-err');

        } else {

            // Iniciar sessão
            Session::login($username);

        }
    }        

}