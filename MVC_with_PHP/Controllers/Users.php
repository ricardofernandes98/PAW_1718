<?php

class Users extends Controller {
    
    /**
     * Função que permite obter todos os utilizadores existentes na base de dados
     */
    public static function findAll() {
        $query = "SELECT * FROM users";
        return Database::query($query);
    }

    /**
     * Função que permite obter um utilizador especifico da base de dados
     */
    public static function findOne($username, $password = '') {
        // A query depende se a password tenha sido passada como argumento da função
        if (!empty($password)) {

            // A password na base de dados encontra-se encriptada
            // Por isso preciso obter o resultado da combinação entre username e password
            $atualPassword = md5($username.$password);
            $query = "SELECT username, password FROM users WHERE username = '".$username."' AND password = '".$atualPassword."'";       
        
        } else {

            // Query que obtem o utilizador sem precisar da password
            $query = "SELECT * FROM users WHERE username = '".$username."'";
        
        }
        // Obter o utilizador da base de dados
        return Database::query($query);
    }

}