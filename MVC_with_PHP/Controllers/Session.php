<?php

class Session extends Controller {

    /**
     * Função que permite iniciar a sessão de um utilizador
     */
    public static function login($username) {
        // Iniciar sessão
        session_start();

        // Variáveis da sessão
        $_SESSION['username'] = $username;
        $_SESSION['login']    = 1;

        header('Location: index.php');
    }

    /**
     * Função que permite terminar a sessão de um utilizador
     */
    public static function logout() {
        // Iniciar sessão
        session_start();

        // Limpar as variáveis da sessão
        session_unset();

        // Destruir a sessão
        session_destroy();  

        // Redirecionar para a página inicial
        header('Location: index.php');
    }

}