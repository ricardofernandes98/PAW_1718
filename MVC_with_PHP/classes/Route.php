<?php

class Route {

    private $routes = array();

    /**
     * Função que permite criar uma rota para a aplicação e associar uma
     * função que será executada aquando da chamada da rota
     */
    public static function set($route, $function) {
        $routes[] = $route;

        // Caso a rota seja chamada a função é executada
        if($_GET['url'] == $route) {
            $function->__invoke();
        }
    }
}