<?php

class Database {

    // Criar a connecção com a base de dados
    private static function connect() {
        $pdo = new PDO("mysql:host=localhost;dbname=DBExample", "root", null);
        return $pdo;
    }

    // Executar uma querie na base de dados
    public static function query($query) {
        $statement = self::connect()->query($query);

        if (explode(' ', $query)[0] == 'SELECT') {
            $data = $statement->fetchAll();
            return $data;
        }

    }
    
}