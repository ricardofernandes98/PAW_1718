<?php
    session_start();

    // Nome da página
    $page = 'Login';
    
    // Incluir o topo
    include_once 'topo.php';
?>
    <!-- Corpo principal da página de login -->

        <div class="login-page">

        <?php if (isset($_SESSION['login'])) { ?>

            <h3>Já se encontra com a sessão inciada. Nome da sessão: <?= $_SESSION['username'] ?>.</h3>

        <?php } else { ?>    

            <header class="login-title">
                <h1>Login</h1>
            </header>

            <div class="login-content">

                <form action="login-user" method="POST">
                    <span>Username</span>
                    <input type="text" name="username" placeholder="Username" minlength="4" required>
                    <br>
                    <span>Password</span>
                    <input type="password" name="password" placeholder="Password" minlength="4" required>
                    <br>
                    <button type="Submit">Login</button>
                </form>

            <div>

            <div class="login-regist">
                <h5>Ainda não se registou? Faça agora o seu <a href="regist">registo</a>.</h5>
            </div>

        <?php } ?>

        </div>

    </body>
    
</html>