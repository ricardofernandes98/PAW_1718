<?php 

    $url_site = "http://localhost/DatabaseTest/";

?>

<!DOCTYPE html>

<html lang="pt">

    <head>

        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
        <!-- O titulo é apresentado consoante o nome da página ativa -->
        <title><?= $page ?></title>
        <link rel="stylesheet" href="../src/css/index.css"/>
    
    </head>

    <body>

        <!-- Mostrar o nome do username caso a sessão esteja iniciada -->
        <?php if (isset($_SESSION['login'])) { ?>

            <div class="session">

                <span id="session-name"><?= $_SESSION['username'] ?><span>

                <form action="logout" method="POST">
                    <input type="hidden" name="username" value="<?= $_SESSION['username'] ?>">
                    <button id="logout" type="submit" id="botao-logout">Logout</button>
                <form>

            </div>

        <?php } ?>

            <div class="menu-nav">
                
                <a href="index.php" id="pagina-inicial" class="<?php if($page == 'Inicio') echo 'active'; ?>">
                    <span>Página Inicial</span>
                </a>
                <a href="regist" id="registo" class="<?php if($page == 'Registo') echo 'active'; ?>">
                    <span>Registo</span>
                </a>
                <a href="login" id="login" class="<?php if($page == 'Login') echo 'active'; ?>">
                    <span>Login</span>
                </a>               
                <a href="show-users" id="show-users" class="<?php if($page == 'Lista de utilizadores') echo 'active'; ?>">
                    <span>Users</span>
                </a> 

            </div>    