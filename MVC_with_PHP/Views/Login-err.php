<?php
    session_start();

    // Nome da página
    $page = 'Erro no login';
    
    // Incluir o topo
    include_once 'topo.php';
?>
    <!-- Corpo principal da página de erro de login -->

        <div class="login-err-content">
        
            <h3>Username ou palavra-passe erradas.</h3>
            <h4>Efetue novamente o <a href="login">login</a> ou <a href="regist">faça o registo</a>.</h4>
        
        </div>

    </body>

</html>
