<?php
    session_start();

    // Nome da página
    $page = 'Inicio';
    
    // Incluir o topo
    include_once 'topo.php';
?>
    <!-- Corpo principal da página incial -->

        <?php if (!isset($_SESSION['login'])) { ?>
        
            <div class="index-page">

                <header class="index-title">
                    <h1>Bem vindo</h1>
                </header>

                <div class="index-content">

                    <button id="botao-login">
                        <a href="login">Login</a>
                    </button> 

                    <button id="botao-registo">
                        <a href="regist">Registe-se</a>
                    </button>
                    
                <div>

            </div>

        <?php } else { ?>

            <div class="index-page">

                <h3>Bem vindo <?php echo $_SESSION['username']; ?></h3>
                
            </div>

        <?php } ?>    

    </body>
    
</html>