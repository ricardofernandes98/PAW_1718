/*
    Operação básica de trocar a posição das imagens:

function subToPrincipal(number) {

    let im = document.getElementById("id1");
    im.src = 'imgs/img_'+ number + '.png';
}
*/

function trocarPosicao(number) {
    document.getElementById("id1").src = 'imgs/img_'+ number + '.png';
}
 
/*
    Eventos associados às imagens
*/
document.getElementById("_id1").addEventListener("click", function() {
    trocarPosicao(2);
});
document.getElementById("_id2").addEventListener("click", function() {
    trocarPosicao(3);
    
});
document.getElementById("_id3").addEventListener("click", function() {
    trocarPosicao(4);
});
document.getElementById("_id4").addEventListener("click", function() {
    trocarPosicao(5);
});
