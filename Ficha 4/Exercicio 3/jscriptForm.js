/**
 * AÇÃO PRINCIPAL:
 * 
 * Função que, ao carregar o botão de confirmação, guarda todas as informações 
 * relativas a um novo utilizador
 */
document.getElementById("botaoConfirmacao").addEventListener("click", function () {
    
    let utilizador = saveFormulario();
    /* getInformacoes(utilizador); */
    document.getElementById("dados").innerHTML = "wwwwwwwww";
});



function saveFormulario() {
    let i = 0;
    let utilizador = [];
    
    if(confirmaTermos() == true) {
        utilizador[i] = {
            name: getNome(),
            apelido: getApelido(),
            sexo: getGenero(),
            mail: getMail(),
            codPostal: getCodPostal(),
            cidade: getCidade(),
            telefone: getTelefone(),
            telemovel: getTelemovel(),
            dataNasc: getDataNasc(),
            password: getPassword()
        }
        i++;
        return utilizador;
    } else {
        alert("Não aceitou os termos");
    }
}

function getInformacoes(user) {

    for (let i = 0; i < 5; i++) {
        document.getElementById("dados").innerHTML= user.name;
    }
}

/**
 * Função que obtem o género do utilizador
 * @returns genero selecionado 
 */
function getGenero() {

    if (document.getElementById("genero1").checked) {
        return document.getElementById("genero1").value;
    } else if (document.getElementById("genero2").checked) {
        return document.getElementById("genero2").value;
    }
}

/**
 * Função que obtem o nome do utilizador
 * @returns nome do utilizador
 */
function getNome() {
    return document.getElementById("nome").value;
}

/**
 * Função que obtem o apelido do utilizador
 * @returns apelido do utilizador
 */
function getApelido() {
    return document.getElementById("apelido").value;
}

/**
 * Função que obtem o mail do utilizador caso seja válido e
 * a sua confirmação seja verdadeira
 * @returns mail do utilizador ou um alerta no caso de o 
 * mail não ser válido
 */
function getMail() {
    let mail = document.getElementById("mail").value;

    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        if (compareValues(mail, document.getElementById("confirmaMail").value)) {
            return mail;
        } else {
            alert("CONFIRMAÇÃO EMAIL :: campos não compatíveis");
        }
    } else {
        alert('AVISO (MAIL): mail invalido');
    }
}

/**
 * Função que obtem o código postal do utilizador
 * @returns código postal do utilizador
 */
function getCodPostal() {
    return document.getElementById("codPostal").value;
}

/**
 * Função que obtem o cidade do utilizador
 * @returns cidade do utilizador
 */
function getCidade() {
    return document.getElementById("cidade").value;
}

/**
 * Função que obtem o país do utilizador
 * @returns país do utilizador
 */
function getPais() {
}

/**
 * Função que obtem o telefone do utilizador
 * @returns telefone do utilizador
 */
function getTelefone() {
    return document.getElementById("numeroTlf").value;
}

/**
 * Função que obtem o telemóvel do utilizador
 * @returns telemóvel do utilizador
 */
function getTelemovel() {
    return document.getElementById("numeroTlm").value;
}

/**
 * Função que obtem a data de nascimento do utilizador
 * @returns data de nascimento do utilizador
 */
function getDataNasc() {
    return document.getElementById("dataNasc").value;
}

/**
 * Função que obtem a password do utilizador
 * @returns apelido do utilizador
 */
function getPassword() {
    let password = document.getElementById("password").value;

    if (compareValues(password, document.getElementById("confirmaPassword").value)) {
        return password;
    } else {
        alert("CONFIRMAÇÃO PASSWORD :: campos não compatíveis")
    }
}

/**
 * Função que obtem o número da carta de condução do utilizador
 * @returns número da carta de condução do utilizador
 */
function getCartaConducao() {
    return document.getElementById("cartaConducao").value;
}

/**
 * Função que verifica se os termos e condições foram aceites
 * @returns true caso foi aceite ou false caso os ters não tenha
 */
function confirmaTermos() {
    if (document.getElementById("validacaoTermos").checked) {
        return true;
    } else {
        return false;
    }
}

/**
 * Função que verifica se dois campos de dados são compatíveis  
 * @param {*} value1 
 * @param {*} value2 
 */
function compareValues(value1, value2) {
    if (value1 == value2) {
        return true;
    } else {
        return false;
    }
}

document.getElementById("confirmaMail").addEventListener('blur', function(){
    let mail = document.getElementById("mail");
    let cMail = document.getElementById("confirmaMail");

    let compativel = compareValues(mail.value, cMail.value);
    if(!compativel) {
        cMail.setCustomValidity("Not compatible!");
    } else {
        cMail.setCustomValidity("");
    }
});

document.getElementById("confirmaPassword").addEventListener('blur', function(){
    let pass = document.getElementById("password");
    let cPass = document.getElementById("confirmaPassword");

    let compativel = compareValues(pass.value, cPass.value);
    if(!compativel) {
        cPass.setCustomValidity("Not compatible!");
    } else {
        cPass.setCustomValidity("");
    }
});

