let orderCount = 0;

const takeOrder = (topping, crustType) => {
  console.log('Order: ' + crustType + 'crust pizza topped with ' + topping);
  orderCount++;
}

function getSubTotal(itemCount) {
  return itemCount * 7.5;
}

takeOrder('cheese', 'bacon');
takeOrder('chesse', 'sauce');
takeOrder('bacon', 'sauce');

console.log(getSubTotal(orderCount));